package org.launchcode.launchcart.controllers;

import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.RollbackException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by LaunchCode
 */
@Controller
public class ItemController extends AbstractCustomerController {

    @Autowired
    private ItemRepository itemRepository;

    @GetMapping("viewItems")
    public String viewAllItems(Model model) {
        model.addAttribute("items", itemRepository.findAll());
        return "item/index";
    }

    @RequestMapping(value = "createItem", method = RequestMethod.GET)
    public String displayCreateNewItemForm(Model model) {
        model.addAttribute(new Item());
        model.addAttribute("title", "New Item");
        return "item/new";
    }

    @RequestMapping(value = "createItem", method = RequestMethod.POST)
    public String createNewItem(@ModelAttribute @Valid Item item, Errors errors, Model model, HttpServletRequest request) {
        if (errors.hasErrors()) {
            model.addAttribute("title", "New Item");
            return "item/new";
        }

        if(item.getDescription() == null){
            item.setDescription("");
        }

        try {
            itemRepository.save(item);
            return "redirect:/viewItems";
        } catch(DataIntegrityViolationException error) {
            model.addAttribute("error", "Cannot add duplicate item.");
            return "item/new";
        }
    }

}
