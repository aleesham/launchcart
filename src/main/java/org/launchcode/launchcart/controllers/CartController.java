package org.launchcode.launchcart.controllers;

import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by LaunchCode
 */
@Controller
public class CartController extends AbstractCustomerController {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @ModelAttribute("cart")
    public Cart getCart() {
        Customer customer = getCustomerFromSession();
        return customer.getCart();
    }

    @RequestMapping(value = "viewCart")
    public String displayCart(){
        return "cart/index";
    }


    @RequestMapping(value = "addItemsToCart", method = RequestMethod.POST)
    public String addItemsToCart(@RequestParam int[] ids, Model model) {
        Customer customer = getCustomerFromSession();
        Cart cart = customer.getCart();
        for (int id : ids) {
            cart.addItem(itemRepository.findOne(id));
        }
        cartRepository.save(cart);
        return "redirect:/viewCart";
    }

    @RequestMapping(value = "removeItemsFromCart", method = RequestMethod.POST)
    public String removeItemsFromCart(@RequestParam int[] ids) {
        Customer customer = getCustomerFromSession();
        Cart cart = customer.getCart();
        for (int id : ids) {
            cart.removeItem(itemRepository.findOne(id));
        }
        cartRepository.save(cart);
        return "redirect:/viewCart";
    }

}
