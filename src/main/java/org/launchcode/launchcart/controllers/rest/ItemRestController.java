package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "/api/items")
public class ItemRestController {

    @Autowired
    ItemRepository itemRepository;

    @GetMapping("")
    public List<Item> getAllItems(@RequestParam Optional<Double> price,
                                  @RequestParam(name = "new") Optional<Boolean> newItem){
        if(price.isPresent() && newItem.isPresent()){
            return itemRepository.findByPriceAndNewItem(price.get(), newItem.get());
        }
        if(price.isPresent()){
            return itemRepository.findByPrice(price.get());
        }
        if(newItem.isPresent()){
            return itemRepository.findByNewItem(newItem.get());
        }
        return itemRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity getSingleItem(@PathVariable int id){
        Item item = itemRepository.findOne(id);
        if(item == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity createItem(@RequestBody Item item){
        return new ResponseEntity(itemRepository.save(item), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateItem(@PathVariable int id, @RequestBody Item item){
        if(itemRepository.findOne(id) == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if(id != item.getUid()){
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        return new ResponseEntity(itemRepository.save(item), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteItem(@PathVariable int id){
        if(itemRepository.findOne(id) == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        itemRepository.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
