package org.launchcode.launchcart.data;

import org.launchcode.launchcart.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by LaunchCode
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
    List<Item> findByPrice(Double price);
    List<Item> findByNewItem(Boolean newItem);
    List<Item> findByPriceAndNewItem(Double price, Boolean newItem);
}
