package org.launchcode.launchcart.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LaunchCode
 */
@Entity
public class Cart extends AbstractEntity {

    @ManyToMany
    private List<Item> items = new ArrayList<>();

    @OneToOne(mappedBy = "cart")
    private Customer owner;

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public double computeTotal() {
        double total = 0;
        for (Item item : items) {
            total += item.getPrice();
        }
        return total;
    }
}
