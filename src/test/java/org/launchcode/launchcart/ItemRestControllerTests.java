package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ItemRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void createTestItems() {
        Item item1 = new Item("Test Item 1", 1);
        itemRepository.save(item1);
        Item item2 = new Item("Test Item 2", 2);
        itemRepository.save(item2);
    }

    @Test
    public void testGetAllItems() throws Exception {
        List<Item> items = itemRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/items"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(items.size())));
        for (int i = 0; i < items.size(); i++) {
            res.andExpect(jsonPath("$[" + i + "].uid", is(items.get(i).getUid())));
        }
    }

    @Test
    public void testGetSingleItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        mockMvc.perform(get("/api/items/{id}", anItem.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(anItem.getUid()));
    }

    @Test
    public void testGetNotFoundItem() throws Exception {
        mockMvc.perform(get("/api/items/-1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPostItem() throws Exception {
        String json = json(new Item("New Item", 42));
        mockMvc.perform(post("/api/items/")
                .content(json)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testPutItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        String oldName = anItem.getName();
        String newName = "New item name";
        anItem.setName(newName);
        String json = json(anItem);
        anItem.setName(oldName);
        mockMvc.perform(put("/api/items/{id}", anItem.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("name").value(newName));
    }

    @Test
    public void testPutNotFoundItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        String json = json(anItem);
        mockMvc.perform(put("/api/items/{id}", -1)
                .content(json).contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPutWithWrongId() throws Exception {
        Item item1 = itemRepository.findAll().get(0);
        Item item2 = itemRepository.findAll().get(1);
        String json = json(item1);
        mockMvc.perform(put("/api/items/{id}", item2.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isConflict());
    }

    @Test
    public void testDeleteItem() throws Exception {
        Item anItem = itemRepository.findAll().get(0);
        int uid = anItem.getUid();
        mockMvc.perform(delete("/api/items/{id}", uid)
                .contentType(contentType))
                .andExpect(status().isOk());
        assertNull(itemRepository.findOne(uid));
    }

    @Test
    public void testDeleteNotFoundItem() throws Exception {
        mockMvc.perform(delete("/api/items/{id}", -1)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testFilterItemsByPrice() throws Exception {
        itemRepository.save(new Item("42 Item 1", 42));
        itemRepository.save(new Item("42 Item 2", 42));
        mockMvc.perform(get("/api/items?price={price}", 42))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].price", is(42.0)))
                .andExpect(jsonPath("$[1].price", is(42.0)));
    }

    @Test
    public void testFilterItemsByNewItem() throws Exception {
        itemRepository.save(new Item("False Item 2", 42, "", false));
        mockMvc.perform(get("/api/items?new={newItem}", false))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].newItem", is(false)));
    }

    @Test
    public void testFilterItemsByNewItemAndPrice() throws Exception {
        itemRepository.save(new Item("False Item 2", 42, "", false));
        mockMvc.perform(get("/api/items?price=2&new=false"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }
}

