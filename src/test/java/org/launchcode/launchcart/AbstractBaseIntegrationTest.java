package org.launchcode.launchcart;

import org.junit.Before;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by LaunchCode
 */
public abstract class AbstractBaseIntegrationTest {

    @Autowired
    protected CustomerRepository customerRepository;

    protected static final String testUserUsername = "launchcode";
    protected static final String testUserPassword = "learntocode";
    protected static int testUserUid;
    protected Customer testCustomer;

    @Before
    public void setUpCustomer() {
        Customer customer = new Customer(testUserUsername, testUserPassword);
        customerRepository.save(customer);
        testUserUid = customer.getUid();
        testCustomer = customer;
    }
}
