package org.launchcode.launchcart;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CustomerRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void createTestCustomers(){
        Item item1 = new Item("bananas", .79);
        Item item2 = new Item("cherries", 4.99);
        Customer customer1 = new Customer("aleesha", "me");
        Customer customer2 = new Customer("nikita", "you");
        customer1.getCart().addItem(item1);
        customer2.getCart().addItem(item1);
        customer2.getCart().addItem(item2);
        customerRepository.save(customer1);
        customerRepository.save(customer2);
    }

    @Test
    public void getAllCutomers() throws Exception {
        List<Customer> customers = customerRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/customers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(customers.size())));
        for (int i = 0; i < customers.size(); i++) {
            res.andExpect(jsonPath("$[" + i + "].uid", is(customers.get(i).getUid())));
        }
    }

    @Test
    public void getSingleCustomer() throws Exception {
        Customer customer = customerRepository.findAll().get(0);
        mockMvc.perform(get("/api/customers/{id}", customer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(customer.getUid()));
    }

    @Test
    public void getNotFoundCustomer() throws Exception {
        Customer customer = customerRepository.findAll().get(0);
        mockMvc.perform(get("/api/customers/{id}", -1))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getCustomerCart() throws Exception {
        Customer customer = customerRepository.findAll().get(0);
        mockMvc.perform(get("/api/customers/{id}/cart", customer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(customer.getCart().getUid()))
                .andExpect(jsonPath("items[0].name").value(customer.getCart().getItems().get(0).getName()));
    }

    @Test
    public void getNotFoundCustomerCart() throws Exception {
        Customer customer = customerRepository.findAll().get(0);
        ResultActions res = mockMvc.perform(get("/api/customers/{id}/cart", -1))
                .andExpect(status().isNotFound());
    }
}
