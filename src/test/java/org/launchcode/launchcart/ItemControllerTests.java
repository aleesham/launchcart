package org.launchcode.launchcart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.controllers.AuthenticationController;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.text.StringContainsInOrder.stringContainsInOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ItemControllerTests extends AbstractBaseIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void testItemIndexListsAllItems() throws Exception {

        itemRepository.save(new Item("Test Item 1", 1));
        itemRepository.save(new Item("Test Item 2", 1));
        itemRepository.save(new Item("Test Item 3", 1));
        itemRepository.save(new Item("Test Item 4", 1));

        List<Item> items = itemRepository.findAll();
        List<String> names = new ArrayList<>();
        for (Item item : items) {
            names.add(item.getName());
        }
        mockMvc.perform(get("/viewItems")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk())
                .andExpect(content().string(stringContainsInOrder(names)));
    }

    @Test
    public void testItemIndexListsAllItemPrices() throws Exception {

        itemRepository.save(new Item("Test Item 1", 1));
        itemRepository.save(new Item("Test Item 2", 1));
        itemRepository.save(new Item("Test Item 3", 1));
        itemRepository.save(new Item("Test Item 4", 1));

        List<Item> items = itemRepository.findAll();
        List<String> prices = new ArrayList<>();
        for (Item item : items) {
            prices.add( ( (Double) item.getPrice() ).toString());
        }

        mockMvc.perform(get("/viewItems")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk())
                .andExpect(content().string(stringContainsInOrder(prices)));
    }

    @Test
    public void testNewItemFormRenders() throws Exception {
        mockMvc.perform(get("/createItem")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("New Item")))
                .andExpect(content().string(containsString("<form")));
    }

    @Test
    public void testNewItemFormCreatesItemAndRedirects() throws Exception {
        String itemName = "Test Item Save";
        mockMvc.perform(post("/createItem")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid)
                .param("name", itemName)
                .param("price", "5")
                .param("description", "A really great item")
                .param("newItem", "true"))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/viewItems"));
        mockMvc.perform(get("/viewItems")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(itemName)));
        Item newItem = itemRepository.findAll().get(0);
        assertTrue(newItem.isNewItem());
    }

    @Test
    public void testItemPricesAreDisplayed() throws Exception {
        itemRepository.save(new Item("Test Item 1", 1));
        itemRepository.save(new Item("Test Item 2", 2));
        itemRepository.save(new Item("Test Item 3", 3));
        Function<Double, String> formatCurrency = (price) -> { return "$" + price;};
        ResultActions res = mockMvc.perform(get("/viewItems")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
                .andExpect(status().isOk());
        for (Item item : itemRepository.findAll()) {
            String price = formatCurrency.apply(item.getPrice());
            res.andExpect(content().string(containsString(price)));
        }
    }

    @Test
    public void addingDuplicateItemDisplaysError() throws Exception {
        String itemName = "Test Item Save",
               error = "Cannot add duplicate item.";

        itemRepository.save(new Item(itemName, 1, "", true));

        mockMvc.perform(post("/createItem")
                .sessionAttr(AuthenticationController.customerSessionKey, testUserUid)
                .param("name", itemName)
                .param("price", "1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(error)));
    }

    @Test
    public void checkButtonStartsDisabled() throws Exception {

        itemRepository.save(new Item("Test Item 1", 1));
        itemRepository.save(new Item("Test Item 2", 1));

        mockMvc.perform(get("/viewItems")
            .sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("<button disabled")));
    }
}
