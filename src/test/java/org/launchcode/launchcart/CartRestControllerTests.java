package org.launchcode.launchcart;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CartRestControllerTests extends AbstractBaseRestIntegrationTest{

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CartRepository cartRepository;

    @Before
    public void createTestItems() {
        //Why are the UIDs being set to zero?
        Item item1 = new Item("Test Item 1", 1);
        Item item2 = new Item("Test Item 2", 2);
        Cart cart1 = new Cart();
        Cart cart2 = new Cart();
        cart1.addItem(item1);
        cart1.addItem(item2);
        cart2.addItem(item1);
        cartRepository.save(cart1);
        cartRepository.save(cart2);
    }

    @Test
    public void getAllCarts() throws Exception {
        List<Cart> carts = cartRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/carts"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(carts.size())));
        for (int i = 0; i < carts.size(); i++) {
            res.andExpect(jsonPath("$[" + i + "].uid", is(carts.get(i).getUid())));
        }
    }

    @Test
    public void testGetSingleCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        mockMvc.perform(get("/api/carts/{id}", cart.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(cart.getUid()));
    }

    @Test
    public void testGetNotFoundCart() throws Exception {
        mockMvc.perform(get("/api/carts/-1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPutItemInCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = new Item("New Item", 5);
        cart.addItem(item);
        String json = json(cart);
        cart.removeItem(item);

        assertEquals(cart.getItems().size(), 2);

        mockMvc.perform(put("/api/carts/{id}", cart.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("items["+ (cart.getItems().size()-1) + "].name").value("New Item"));


        assertEquals(cart.getItems().size(), 3);
    }

    @Test
    public void testPutNotFoundInCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = new Item("New Item", 5);
        cart.addItem(item);
        String json = json(item);
        cart.removeItem(item);

        mockMvc.perform(put("/api/carts/{id}", -1)
                .content(json).contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPutWithWrongId() throws Exception {
        Cart cart1 = cartRepository.findAll().get(0);
        Cart cart2 = cartRepository.findAll().get(1);
        String json = json(cart1);

        mockMvc.perform(put("/api/carts/{id}", cart2.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isConflict());
    }

    @Test
    public void testDeleteItemInCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = cart.getItems().get(0);
        cart.removeItem(item);
        String json = json(cart);
        cart.addItem(item);

        assertEquals(cart.getItems().size(), 2);
        mockMvc.perform(put("/api/carts/{id}", cart.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("items[0].name").value("Test Item 2"));
        assertEquals(cart.getItems().size(), 1);
    }
}
